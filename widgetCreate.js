﻿function includeHTML(data) {

    let partnerPhoneHtml = ('<a href="tel:+' + data.countryCode + '-' + data.phone + '"> +' + data.countryCode + '-' + data.phone + ' </a>')
    document.getElementById('partnerPhone').innerHTML = partnerPhoneHtml;


    let partnerEmailHtml = ('<a href="mailto:' + Date.email + '"> ' + data.email + ' </a>')
    document.getElementById('partnerEmail').innerHTML = partnerEmailHtml;

    let partnerLogoHtml = ('<a href="#" class="navbar-brand pull-right"> ' + '<img border="0" width="64px" height="32px" src="' + data.partner_logo_url + '"></a>');
    document.getElementById('partner-logo').innerHTML = partnerLogoHtml;

    document.getElementById('partnerCode').innerHTML = data.code;
    document.getElementById('mobileTrackingUrl').innerHTML = data.partnerMobileUrl;
}

// partner details
let data = {
    "name": "Siliconveins PVT. Ltd.",
    "partner_logo_url": "widget-img/partner-logo.jpg",
    "code": 'SVEINS',
    "countryCode": "92",
    "phone": "123456789",
    "email": "partner@gmail.com",
    "partnerMobileUrl": 'https://tallymobile.app/?' + 'SVEINS'
}

includeHTML(data)